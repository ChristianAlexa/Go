import React, { Component } from "react";
import Tile from "../Tile/Tile";
import Stone from "../Stone/Stone";
import "./Board.css";

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Go",
      moveNum: 0,
      turn: "black",
      tiles: [],
      error: "",
      history: [],
      capturedWhiteStones: [],
      capturedBlackStones: [],
      allGroups: []
    };

    // generate game board tiles
    for (let i = 0; i < 361; i++) {
      if (this.determinePosition(i) === "CORNER") {
        this.state.tiles.push(
          <Tile
            key={i}
            value={null}
            click={() => this.clickHandler(i)}
            num={i}
            libertyCount={2}
            inAtari={false}
          />
        );
      } else if (this.determinePosition(i) === "MIDDLE") {
        this.state.tiles.push(
          <Tile
            key={i}
            value={null}
            click={() => this.clickHandler(i)}
            num={i}
            libertyCount={4}
            inAtari={false}
          />
        );
      } else {
        this.state.tiles.push(
          <Tile
            key={i}
            value={null}
            click={() => this.clickHandler(i)}
            num={i}
            libertyCount={3}
            inAtari={false}
          />
        );
      }
    }
  }

  determinePosition = i => {
    const TOP = "TOP";
    const RIGHT = "RIGHT";
    const BOTTOM = "BOTTOM";
    const LEFT = "LEFT";
    const CORNER = "CORNER";
    const MIDDLE = "MIDDLE";

    if (i === 0 || i === 18 || i === 342 || i === 360) return CORNER;
    else if (i >= 1 && i <= 17) return TOP;
    else if (i >= 343 && i <= 359) return BOTTOM;
    else if (i % 19 === 0 && i !== 342 && i !== 0) return LEFT;
    else if ((i + 1) % 19 === 0 && i !== 18 && i !== 360) return RIGHT;
    else return MIDDLE;
  };

  getNeighbors = (i, tiles) => {
    let left = i - 1;
    let above = i - 19;
    let right = i + 1;
    let below = i + 19;
    let position = this.determinePosition(i);
    let neighbors = [];

    if (position === "TOP") {
      neighbors.push(tiles[right], tiles[left], tiles[below]);
      return neighbors;
    } else if (position === "BOTTOM") {
      neighbors.push(tiles[right], tiles[left], tiles[above]);
      return neighbors;
    } else if (position === "LEFT") {
      neighbors.push(tiles[above], tiles[right], tiles[below]);
      return neighbors;
    } else if (position === "RIGHT") {
      neighbors.push(tiles[above], tiles[left], tiles[below]);
      return neighbors;
    } else if (position === "MIDDLE") {
      neighbors.push(tiles[above], tiles[left], tiles[right], tiles[below]);
      return neighbors;
    } else if (position === "CORNER") {
      if (i === 0) {
        neighbors.push(tiles[right], tiles[below]);
        return neighbors;
      } else if (i === 18) {
        neighbors.push(tiles[left], tiles[below]);
        return neighbors;
      } else if (i === 342) {
        neighbors.push(tiles[right], tiles[above]);
        return neighbors;
      } else {
        neighbors.push(tiles[above], tiles[left]);
        return neighbors;
      }
    }
  };

  hasStone = (i, tiles) => (tiles[i].props.value != null ? true : false);

  generateTile = (i, libertyCount, color, atariStatus) => {
    let newTile = (
      <Tile
        key={i}
        value={color}
        click={() => this.clickHandler(i)}
        num={i}
        libertyCount={libertyCount}
        inAtari={atariStatus}
      />
    );
    return newTile;
  };

  toggleTurn = thisTurn => (thisTurn === "black" ? "white" : "black");

  increaseMoveNum = currentMoveNum => ++currentMoveNum;

  hasLiberties = neighbors => {
    for (let i = 0; i < neighbors.length; i++) {
      if (neighbors[i].props.value === null) {
        return true;
      }
    }
    return false;
  };

  neighborHasFriends = (neighborIndex, neighbors) => {
    let friendCount = 0;
    let hasFriends = false;
    let tiles = this.state.tiles;
    console.log(neighbors);

    // for each of the neighbor neighbors
    for (let j = 0; j < neighbors.length; j++) {
      if (neighbors[j].props.value !== null) {
        console.log("COLOR: " + neighbors[j].props.value);
        if (neighbors[j].props.value === tiles[neighborIndex].props.value) {
          console.log("friend found!");
          friendCount++;
        }
      }
    }
    if (friendCount > 0) {
      hasFriends = true;
    }
    return hasFriends;
  };

  clickedTileHasFriends = neighbors => {
    let friendCount = 0;
    let hasFriends = false;
    // for each of the neighbor's neighbor
    for (let j = 0; j < neighbors.length; j++) {
      // if neighbor's color is the same as tile i's color return true
      if (
        neighbors[j].props.value === this.state.turn &&
        neighbors[j].props.value !== null
      ) {
        // console.log("neighbor color: " + neighbors[j].props.value);
        // console.log("clicked tile color: " + this.state.turn);
        friendCount++;
      }
    }
    // console.log("Friend count: " + friendCount);

    if (friendCount > 0) {
      hasFriends = true;
    }
    return hasFriends;
  };

  anyStoneNeighborCount = neighbors => {
    let anyStoneNeighborCount = 0;
    for (let i = 0; i < neighbors.length; i++) {
      if (neighbors[i].props.value !== null) {
        anyStoneNeighborCount++;
      }
    }
    return anyStoneNeighborCount;
  };

  baseLiberty = position => {
    let libertyCount = 0;
    if (position === "CORNER") {
      libertyCount = 2;
    } else if (position === "MIDDLE") {
      libertyCount = 4;
    } else {
      libertyCount = 3;
    }
    return libertyCount;
  };

  getCurrentGroup = tileIndex => {
    let tiles = this.state.tiles;
    let allGroups = this.state.allGroups;
    let currentGroup = null;

    // for each group in all groups
    for (let group = 0; group < allGroups.length; group++) {
      // for each tile in a group
      for (let tile = 0; tile < allGroups[group].length; tile++) {
        // if the tile's unique number is identified in a group
        // console.log("Tile index : " + allGroups[group][tile].props.num);
        if (allGroups[group][tile].props.num === tiles[tileIndex].props.num) {
          // current group of the passed in tile index has been found!
          currentGroup = allGroups[group];
          break;
        }
      }
    }

    return currentGroup;
  };

  getCurrentGroupIndex = index => {
    let tiles = this.state.tiles;
    let allGroups = this.state.allGroups;
    let spliceIndex = null;

    for (let g = 0; g < allGroups.length; g++) {
      for (let t = 0; t < allGroups[g].length; t++) {
        if (allGroups[g][t].props.num === tiles[index].props.num) {
          spliceIndex = g;
          break;
        }
      }
    }

    return spliceIndex;
  };

  currentGroupLibertyCount = currentGroup => {
    let sum = 0;

    // for each tile in the current group
    for (let i = 0; i < currentGroup.length; i++) {
      sum += currentGroup[i].props.libertyCount;
    }

    return sum;
  };

  clickHandler = i => {
    let tiles = this.state.tiles;
    let error = this.state.error;
    let allGroups = this.state.allGroups;

    /********************************************
     *
     * IS THE TILE OCCUPIED?
     *
     ********************************************/

    if (this.hasStone(i, tiles)) {
      // OCCUPIED
      error = "Oops, there's a stone there!";
      this.setState({
        error: error
      });
    } else {
      // NOT OCCUPIED
      console.log("You CLICKED tile " + i);
      error = "";

      // COLLECT DATA FOR POTENTIAL STONE PLACEMENT
      let position = this.determinePosition(i);
      let neighbors = this.getNeighbors(i, tiles);
      let hasLiberties = this.hasLiberties(neighbors);
      let baseLibertyCount = this.baseLiberty(position);
      let hasFriends = this.clickedTileHasFriends(neighbors);
      let anyStoneNeighborCount = this.anyStoneNeighborCount(neighbors);

      /********************************************
       *
       * CAN THE STONE BE PLACED HERE LEGALLY?
       *
       ********************************************/
      if (hasLiberties === false && hasFriends === false) {
        // NOT ALLOWED TO PLAY HERE
        error = "Suicidal move!";
        this.setState({
          error: error
        });
      } else {
        // ALLOWED TO PLAY HERE!

        let moveCount = this.increaseMoveNum(this.state.moveNum);
        let nextTurn = this.toggleTurn(this.state.turn);

        /********************************************
         *
         * CREATE NEW CLICKED TILE
         *
         ********************************************/

        // reduce the liberty of the placed stone given the situation
        let adjustedLibCount = baseLibertyCount - anyStoneNeighborCount;

        // create a new clicked tile
        let newClickedTile = this.generateTile(
          i,
          adjustedLibCount,
          this.state.turn,
          false
        );

        tiles[i] = newClickedTile;

        /********************************************
         *
         * CREATE and MERGE stone groupings
         *
         ********************************************/

        let newGroup = [newClickedTile];
        allGroups.push(newGroup);
        let friendlyGroup = [];
        let friendlyNeighbors = [];

        if (hasFriends) {
          // build friendly neighbor list
          for (let k = 0; k < neighbors.length; k++) {
            if (neighbors[k].props.value === newClickedTile.props.value) {
              friendlyNeighbors.push(neighbors[k]);
            }
          }
          // for each friendly neighbor, merge into a single group
          for (let n = 0; n < friendlyNeighbors.length; n++) {
            let spliceIndex = null;
            let inGroup = false;

            // is the tile already present in the new group?
            for (let f = 0; f < newGroup.length; f++) {
              if (newGroup[f].props.num === friendlyNeighbors[n].props.num) {
                inGroup = true;
              }
            }

            // If the neighbor is NOT in the clicked tile's grouping
            if (inGroup === false) {
              // for each group in all groups
              for (let x = 0; x < allGroups.length; x++) {
                // for each tile in a group
                for (let t = 0; t < allGroups[x].length; t++) {
                  // if the unique tile is found
                  if (
                    allGroups[x][t].props.num === friendlyNeighbors[n].props.num
                  ) {
                    // friendly group found
                    friendlyGroup = allGroups[x];

                    // grab the group index to remove from all groups later
                    spliceIndex = x;
                    break;
                  }
                }
              }

              // for each tile in the firendly group, merge tile into new group
              for (let g = 0; g < friendlyGroup.length; g++) {
                newGroup.push(friendlyGroup[g]);
              }
              // remove friendly group from all Groups
              allGroups.splice(spliceIndex, 1);
            }
          }
        }

        console.log(allGroups);

        /********************************************
         *
         * CREATE NEW NEIGHBOR TILES OF THE CLICKED TILE
         *
         ********************************************/

        // for each neighbor tile
        for (let j = 0; j < neighbors.length; j++) {
          // if neighbor is a STONE
          if (neighbors[j].props.value !== null) {
            let adjustedNeighborLibCount = neighbors[j].props.libertyCount;
            let neighborIndex = neighbors[j].props.num;
            let neighborValue = neighbors[j].props.value;
            let neighborPosition = this.determinePosition(j);
            let neighborStartingLib = this.baseLiberty(neighborPosition);
            let neighborsNeighbors = this.getNeighbors(neighborIndex, tiles);
            // let neighborHasFriends = this.hasFriends(
            //   neighborIndex,
            //   neighborsNeighbors
            // );
            let neighborHasFriends = this.neighborHasFriends(
              neighborIndex,
              neighborsNeighbors
            );
            let neighborHasLiberties = this.hasLiberties(neighborsNeighbors);

            // reduce stone liberty count by 1
            adjustedNeighborLibCount--;
            // console.log(neighborsNeighbors);
            // console.log("neighbor index: " + neighborIndex);
            // console.log("neighbor lib count: " + adjustedNeighborLibCount);
            console.log("neighbor has friends? " + neighborHasFriends);
            // console.log("neighbor has liberties " + neighborHasLiberties);

            // determine atari status
            let inAtari = false;
            let currentGroup = this.getCurrentGroup(neighborIndex);
            let currentGroupLibCount = this.currentGroupLibertyCount(
              currentGroup
            );
            if (currentGroupLibCount === 1) {
              inAtari = true;
            }

            let newNeighborTile = this.generateTile(
              neighborIndex,
              adjustedNeighborLibCount,
              neighborValue,
              inAtari
            );
            tiles[neighborIndex] = newNeighborTile;

            for (let r = 0; r < currentGroup.length; r++) {
              if (currentGroup[r].props.num === neighborIndex) {
                currentGroup[r] = newNeighborTile;
              }
            }

            this.setState({
              tiles: [...this.state.tiles],
              allGroups: [...this.state.allGroups]
            });

            /********************************************
             *
             * CASES IF NEIGHBORS ARE IN ATARI, DEAD, OR LIVING
             *
             ********************************************/
            if (
              adjustedNeighborLibCount === 0 &&
              neighborHasFriends === false &&
              neighborHasLiberties === false
            ) {
              console.log("capturing...");
              // the neighbor stone was captured...
              neighborValue = null;
              adjustedNeighborLibCount = neighborStartingLib;

              /********************************************
               * ADD BACK 1 LIBERTY TO ALL OF THE
               * CAPUTRED STONE'S NEIGHBORS
               *
               ********************************************/

              // find neighbors of the captured stone
              let capturedNeighbors = this.getNeighbors(neighborIndex, tiles);

              // loop through the captured stone's neighbors
              for (let k = 0; k < capturedNeighbors.length; k++) {
                // if it's a stone
                if (capturedNeighbors[k].props.value !== null) {
                  // add back a liberty
                  let adjustedCapturedNeighborLib =
                    capturedNeighbors[k].props.libertyCount;
                  adjustedCapturedNeighborLib++;

                  // make a new replacemnt tile for the neighbor of the capture stone
                  let newCapturedNeighborTile = this.generateTile(
                    capturedNeighbors[k].props.num,
                    adjustedCapturedNeighborLib,
                    capturedNeighbors[k].props.value,
                    false
                  );

                  // insert into the tiles
                  tiles[
                    capturedNeighbors[k].props.num
                  ] = newCapturedNeighborTile;

                  // determine atari status
                  let inAtari = false;
                  let currentGroup = this.getCurrentGroup(
                    capturedNeighbors[k].props.num
                  );
                  let currentGroupLibCount = this.currentGroupLibertyCount(
                    currentGroup
                  );
                  if (currentGroupLibCount === 1) {
                    inAtari = true;
                  }

                  // CREATE A NEW NEIGHBOR TILE
                  let newNeighborTile = this.generateTile(
                    neighborIndex,
                    neighborStartingLib,
                    neighborValue,
                    inAtari
                  );

                  // insert new neighbor tile into tiles
                  tiles[neighborIndex] = newNeighborTile;

                  this.setState({
                    tiles: [...this.state.tiles]
                  });
                }
              }
            } else if (
              adjustedNeighborLibCount === 0 &&
              neighborHasFriends === true
            ) {
              // neighbor stone could be captured and is looking to its group to save it
              let currentGroup = this.getCurrentGroup(neighborIndex);
              let libertyCount = 0;

              for (let g = 0; g < currentGroup.length; g++) {
                libertyCount += currentGroup[g].props.libertyCount;
              }

              // if the group has no liberties left
              if (libertyCount === 0) {
                // all of the group has been captured
                console.log("group has been captured");

                // reset all the tiles of the captured group
                for (let i = 0; i < currentGroup.length; i++) {
                  let resetPosition = this.determinePosition(
                    currentGroup[i].props.num
                  );
                  let resetLib = this.baseLiberty(resetPosition);

                  let resetTile = this.generateTile(
                    currentGroup[i].props.num,
                    resetLib,
                    null,
                    false
                  );

                  tiles[currentGroup[i].props.num] = resetTile;

                  this.setState({
                    tiles: [...this.state.tiles]
                  });
                }

                // splice the captured group from all groups
                let currentGroupIndex = this.getCurrentGroupIndex(
                  neighborIndex
                );
                allGroups.splice(currentGroupIndex, 1);
              } else {
                // liberties found, group saved
                console.log("group has been saved");
              }
            } else if (adjustedNeighborLibCount === 1) {
              // attempting to place all stones in the current group in atari
              // console.log("Neighbor index: " + neighborIndex);
              let currentGroup = this.getCurrentGroup(neighborIndex);
              let currentGroupLibCount = this.currentGroupLibertyCount(
                currentGroup
              );
              if (currentGroupLibCount === 1) {
                console.log("current group lib count is 1");
                // for each tile in the current group
                for (let i = 0; i < currentGroup.length; i++) {
                  // generate new tile with inAtari is true
                  let newCurrentGroupTile = this.generateTile(
                    currentGroup[i].props.num,
                    currentGroup[i].props.libertyCount,
                    currentGroup[i].props.value,
                    true
                  );

                  currentGroup[i] = newCurrentGroupTile;

                  // insert each tile of the current group into tiles
                  tiles[currentGroup[i].props.num] = newCurrentGroupTile;

                  // set state
                  this.setState({
                    tiles: [...this.state.tiles],
                    allGroups: [...this.state.allGroups]
                  });
                }
              } else {
                let newNeighborTile = null;

                let currentGroup = this.getCurrentGroup(neighborIndex);
                for (let n = 0; n < currentGroup.length; n++) {
                  // determine atari status
                  let inAtari = false;
                  let currentGroup = this.getCurrentGroup(neighborIndex);
                  let currentGroupLibCount = this.currentGroupLibertyCount(
                    currentGroup
                  );
                  if (currentGroupLibCount === 1) {
                    inAtari = true;
                  }

                  if (currentGroup[n].props.num === neighborIndex) {
                    newNeighborTile = this.generateTile(
                      neighborIndex,
                      adjustedNeighborLibCount,
                      neighborValue,
                      inAtari
                    );

                    currentGroup[n] = newNeighborTile;

                    tiles[neighborIndex] = newNeighborTile;

                    this.setState({
                      tiles: [...this.state.tiles],
                      allGroups: [...this.state.allGroups]
                    });
                  }
                }
              }
              // if a stone was reduced to 1, change every tile in the group to inAtari
            } else {
              // the stone was not captured...(could be friendly or NOT)

              // determine atari status
              let inAtari = false;
              let currentGroup = this.getCurrentGroup(neighborIndex);
              let currentGroupLibCount = this.currentGroupLibertyCount(
                currentGroup
              );
              if (currentGroupLibCount === 1) {
                inAtari = true;
              }

              // CREATE A NEW NEIGHBOR TILE
              let newNeighborTile = this.generateTile(
                neighborIndex,
                adjustedNeighborLibCount,
                neighborValue,
                inAtari
              );

              // insert new neighbor tile into tiles
              tiles[neighborIndex] = newNeighborTile;

              this.setState({
                tiles: [...this.state.tiles]
              });
            }
          }
        }

        this.setState({
          moveNum: moveCount,
          turn: nextTurn,
          error: error,
          history: [...this.state.history, i],
          allGroups: [...this.state.allGroups]
        });
      }
    }
  };

  reset = () => {
    let newTiles = [];
    let allGroups = [];

    for (let i = 0; i < 361; i++) {
      if (this.determinePosition(i) === "CORNER") {
        newTiles.push(
          <Tile
            key={i}
            value={null}
            click={() => this.clickHandler(i)}
            num={i}
            libertyCount={2}
            inAtari={false}
          />
        );
      } else if (this.determinePosition(i) === "MIDDLE") {
        newTiles.push(
          <Tile
            key={i}
            value={null}
            click={() => this.clickHandler(i)}
            num={i}
            libertyCount={4}
            inAtari={false}
          />
        );
      } else {
        newTiles.push(
          <Tile
            key={i}
            value={null}
            click={() => this.clickHandler(i)}
            num={i}
            libertyCount={3}
            inAtari={false}
          />
        );
      }
    }

    this.setState({
      moveNum: 0,
      turn: "black",
      tiles: newTiles,
      error: "",
      history: [],
      capturedWhiteStones: [],
      capturedBlackStones: [],
      allGroups: allGroups
    });

    console.clear();
  };

  render() {
    const playerTurn =
      this.state.turn[0].toUpperCase() + this.state.turn.slice(1);
    const errorStyle = {
      color: "red",
      textAlign: "center"
    };
    const errorContainer = {
      height: "20px"
    };
    const resetBtn = {
      padding: "10px",
      borderRadius: "5px",
      backgroundColor: "#0099e5",
      color: "white",
      cursor: "pointer"
    };
    const turnContainer = {
      height: "30px"
    };
    const history = {
      textAlign: "center",
      width: "630px",
      margin: "0 auto"
    };

    return (
      <div>
        <div className="panel">
          <h1>{this.state.name}</h1>
          <p>Move count: {this.state.moveNum}</p>
          <p>{playerTurn} to play</p>
          <div style={turnContainer}>
            <Stone color={this.state.turn} libertyCount={4} inAtari={false} />
          </div>
          <br />
          <button style={resetBtn} onClick={this.reset}>
            New Game
          </button>
        </div>

        <br />

        <div style={errorContainer}>
          <p style={errorStyle}>{this.state.error}</p>
        </div>
        <div className="board-container">{this.state.tiles}</div>
        <div style={history}>
          <h3>History</h3>
          {this.state.history.map((move, index) => {
            return (
              <span
                style={{ listStyleType: "none", marginLeft: "10px" }}
                key={index}
              >
                {move},{" "}
              </span>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Board;
