import React from "react";
import Creep from "./Creep_tumor.png";
import Atari from "./ditto.png";
// import Explosion from "./explosion.gif_c200";

const Stone = props => {
  let stoneColor = {};
  let creep = null;

  if (props.color === "white") {
    stoneColor = {
      backgroundColor: "#fff",
      borderRadius: "50%",
      border: "1px solid black",
      height: "30px",
      width: "30px",
      textAlign: "center",
      margin: "0 auto"
    };
  }

  if (props.color === "black" && props.inAtari === true) {
    creep = (
      <img style={{ heigh: "35px", width: "35px" }} src={Atari} alt="atari" />
    );
  }
  if (props.color === "black" && props.inAtari === false) {
    creep = (
      <img style={{ height: "30px", width: "30px" }} src={Creep} alt="tumor" />
    );
  }
  return (
    <div>
      <div style={stoneColor} />
      {creep}
    </div>
  );
};

export default Stone;
