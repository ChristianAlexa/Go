import React from "react";
import "./Tile.css";
import Stone from "../Stone/Stone";

const Tile = props => {
  let tileContent = null;
  let tile = "tile";
  let intersection = "intersection";
  let symbol = "+";

  // https://www.alt-codes.net/diamond-symbols
  if (
    props.num === 60 ||
    props.num === 72 ||
    props.num === 288 ||
    props.num === 300
  ) {
    intersection = "star-point";
    symbol = "⟐";
  }

  if (
    props.num === 66 ||
    props.num === 174 ||
    props.num === 186 ||
    props.num === 294 ||
    props.num === 180
  ) {
    intersection = "minor-star-point";
    symbol = "⟐";
  }

  if (props.value === null) {
    tileContent = <div className={intersection}>{symbol}</div>;
  } else {
    tileContent = (
      <Stone
        color={props.value}
        libertyCount={props.libertyCount}
        inAtari={props.inAtari}
      />
    );
  }

  return (
    <div className={tile} onClick={props.click}>
      {tileContent}
    </div>
  );
};

export default Tile;
